<!-- # Getting started -->

<!-- ```{r} -->
<!-- #| echo: false -->
<!-- source('../../knitr-formatting.R') -->
<!-- ``` -->

<!-- ATLAS model is implemented in the `methis.atlas` `R` package -->

<!-- ## Installing the package -->

<!-- ```{r, eval=FALSE} -->
<!-- remotes::install_gitlab('iarc-miarc/methis/methis.atlas') -->
<!-- ``` -->

<!-- ## Simple examples -->

<!-- ### Infering the cumulative risk of cervical cancer in a cohort of girls having 10-14 in 2020 in Rwanda -->

<!-- #### Loading the package and the data -->

<!-- ```{r} -->
<!-- library('methis.atlas') -->
<!-- ``` -->

<!-- Let's load the 2020 cervical cancer incidence rates estimates among Rwandan girls from `methis.atlas` package. -->

<!-- ```{r} -->
<!-- data('dat.gco.ccir.year.2020') -->
<!-- dat.gco.ccir.year.2020 -->
<!-- ``` -->

<!-- This data contains by 5 years age group (`ageg`), the estimated cervical cancer incidence rate in 2020 (`IR_2020`) extracted from GLOBOCAN 2020. -->

<!-- Let's then load the overall death rates estimation for Rwandan girls in 2020. -->

<!-- ```{r} -->
<!-- data('dat.un.mort.year.2020') -->
<!-- dat.un.mort.year.2020 -->
<!-- ``` -->

<!-- This data contains by 5 years age group (`ageg`), the estimated death rate in 2020 (`CDR_2020`) extracted from UN Population Division World Population Prospect in 2022. -->

<!-- This 2 data-set can easily be joined to be sure the age group match correctly. -->

<!-- ```{r} -->
<!-- atlas.dat <- dplyr::full_join(dat.gco.ccir.year.2020, dat.un.mort.year.2020, by = 'ageg') -->
<!-- ``` -->

<!-- #### Defining the prevention scenario -->

<!-- Let's assume that 74% of the cervical cancer are attributable to HPV16/18 and are preventable by bivalent vaccination. The remaining 26 are considered as non-preventable in this example. -->

<!-- Let's then assume that we observed a 80% reduction impact of HPV16/18 due to vaccination campaign implemented in Rwanda. -->

<!-- #### Compute the cumulative risk of cervical cancer with and without intervention -->

<!-- `atlas_lite` function allow you to calculate the cumulative risk of cervical cancer with and without intervention according to some hypothetical public health intervention. -->

<!-- ```{r} -->
<!-- cum.risk.cc.rw <- -->
<!--   atlas_lite( -->
<!--     ageg = atlas.dat$ageg, -->
<!--     IR = atlas.dat$IR_2020, -->
<!--     CDR = atlas.dat$CDR_2020, -->
<!--     prev.in.cancer = .74,  -->
<!--     impact.on.prev = .8, -->
<!--     prop.at.risk.end = FALSE -->
<!--   ) -->
<!-- cum.risk.cc.rw -->
<!-- ``` -->

<!-- We get a 5 column data.frame containing for a cohort of 100 000 individuals, the cumulative number of cancer cases expected in each age group (`ageg`) in the absence of preventive interventions (`LTR.no.vacc`), and considering the given impact of a public health interventions, the number of cancer cases prevented (`LTR.prevented`) and not prevented (`LTR.not.prevented`). The proportion of the initial population still at risk is also returned (`prop.at.risk`). -->

<!-- To obtain the number of cancer case for a different population size, multiply the estimated number of cancer cases by your population size / 100,000. -->

<!-- ```{r} -->
<!-- #| echo: true -->

<!-- cum.nb.no.vacc <- round(cum.risk.cc.rw$LTR.no.vacc[cum.risk.cc.rw$ageg == '80-84']) -->
<!-- cum.nb.prevented <- round(cum.risk.cc.rw$LTR.prevented[cum.risk.cc.rw$ageg == '80-84']) -->
<!-- cum.nb.not.prevented <- round(cum.risk.cc.rw$LTR.not.prevented[cum.risk.cc.rw$ageg == '80-84']) -->
<!-- ``` -->

<!-- In this example if we consider a cohort of 100,000 Rwandan girls aged 10-14 in 2020 we would expect `r as.integer(cum.nb.no.vacc)` cases of cancer without intervention in the coming 70 years but if Rwanda manage to decrease HPV16/18 prevalence by 80% in these cohorts of girls `r as.integer(cum.nb.prevented)` will be prevented. Then we expect still `r as.integer(cum.nb.not.prevented)` cancer cases. -->

<!-- ```{r} -->
<!-- #| echo: true -->

<!-- nb.girls.rw <- 777261 -->
<!-- cum.nb.no.vacc.02 <- round(nb.girls.rw * cum.nb.no.vacc / 100000) -->
<!-- cum.nb.prevented.02 <- round(nb.girls.rw * cum.nb.prevented / 100000) -->
<!-- cum.nb.not.prevented.02 <- round(nb.girls.rw * cum.nb.not.prevented / 100000) -->
<!-- ``` -->

<!-- Which correspond to `r as.integer(cum.nb.prevented.02)` cases prevented over the `r as.integer(cum.nb.no.vacc.02)` expected if no prevention is done over the `r as.integer(nb.girls.rw)` Rwandan girls aged 10-14 in 2020. -->

<!-- #### Plot the cumulative risk of geting a cervical cancer in Rwanda -->

<!-- `ATLAS` outcomes can be translated into cumulative risk of getting a cancer in percentage. We can then display how this cumulative risk is predicted to involve when girls will be aging according to our vaccination scenario. -->

<!-- ```{r} -->
<!-- library(ggplot2) -->
<!-- cum.risk.cc.rw |> -->
<!--   dplyr::select(ageg, LTR.no.vacc, LTR.not.prevented) |> -->
<!--   tidyr::pivot_longer(c('LTR.no.vacc', 'LTR.not.prevented'), names_to = 'scen', values_to = 'LTR') |> -->
<!--   dplyr::mutate(scen = factor(scen, levels = c('LTR.no.vacc', 'LTR.not.prevented'), labels = c('No vaccination', '80% vacc. impact on HPV16/18'))) |> -->
<!--   ggplot(aes(x = ageg, y = LTR / 100000, colour = scen, group = scen)) + -->
<!--   geom_line(linewidth = 1) + -->
<!--   scale_y_continuous('Cumulative Risk of Cervical Cancer', labels = scales::percent) + -->
<!--   labs(x = 'Age Group', colour = 'Scenario') +  -->
<!--   theme_light() + -->
<!--   theme(  -->
<!--     axis.text.x = element_text(angle = 90, vjust = .5) -->
<!--   ) -->
<!-- ``` -->

<!-- We see that even without vaccination girls being 10-14 in 2020 have a risk of `r round(cum.nb.no.vacc/1000, 1)`% to get a cancer in the coming 70 years. This risk should drop to `r round(cum.nb.not.prevented/1000, 1)`% with a 80% reduction of HPV16/18. -->

<!-- This is very important to keep in mind that this estimates are based on the strong assumption that sexual behavior (HPV transmission) and cervical cancer screening policies will remain unchainged in the coming 70 years. -->
